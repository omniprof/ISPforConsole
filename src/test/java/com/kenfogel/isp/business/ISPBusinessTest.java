package com.kenfogel.isp.business;

import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_A;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_B;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_C;
import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.junit.Test;
import static org.junit.Assert.*;

import com.kenfogel.isp.data.ISPBean;

/**
 * A parameterized test class. Requires JUnit 4.11
 *
 * @author Ken
 */
@RunWith(Parameterized.class)
public class ISPBusinessTest {

    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
    @Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {new ISPBean('A', PLANRATE_A, 30), 49.95, 20, 40.0},
            {new ISPBean('A', PLANRATE_A, 40), 69.95, 30, 60.0},
            {new ISPBean('B', PLANRATE_B, 30), 24.95, 10, 10.0},
            {new ISPBean('B', PLANRATE_B, 40), 34.95, 20, 20.0},
            {new ISPBean('C', PLANRATE_C, 30), 19.95, 0, 0},
            {new ISPBean('C', PLANRATE_C, 40), 19.95, 0, 0}
        });
    }

    ISPBean ispBean;
    double expectedTotal;
    double expectedAdditionlHours;
    double expectedAdditionalhoursCharge;

    /**
     * Constructor that receives all the data for each test as defined by a row
     * in the list of parameters
     *
     * @param ispBean
     * @param expectedTotal
     * @param expectedAdditionlHours
     * @param expectedAdditionalhoursCharge
     */
    public ISPBusinessTest(ISPBean ispBean, double expectedTotal, double expectedAdditionlHours, double expectedAdditionalhoursCharge) {
        this.ispBean = ispBean;
        this.expectedTotal = expectedTotal;
        this.expectedAdditionlHours = expectedAdditionlHours;
        this.expectedAdditionalhoursCharge = expectedAdditionalhoursCharge;
    }

    /**
     * Test of getAdditionalHours method, of class ISPBusiness.
     */
    @Test
    public void testGetAdditionalHours() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedAdditionlHours, instance.getAdditionalHours(), 2);
    }

    /**
     * Test of getAdditionalHoursCharge method, of class ISPBusiness.
     */
    @Test
    public void testGetAdditionalHoursCharge() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedAdditionalhoursCharge, instance.getAdditionalHoursCharge(), 2);
    }

    /**
     * Test of getTotalCost method, of class ISPBusiness.
     */
    @Test
    public void testGetTotalCost() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedTotal, instance.getTotalCost(), 2);
    }

}
