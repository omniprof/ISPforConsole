package com.kenfogel.isp.presentation;

import java.text.NumberFormat;
import java.util.Scanner;

import com.kenfogel.isp.business.ISPBusiness;
import static com.kenfogel.isp.constants.ISPConstants.INVALID;
import static com.kenfogel.isp.constants.ISPConstants.REGEX_CHOICES;
import com.kenfogel.isp.data.ISPBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for the presentation layer of the program. It can
 * easily be replaced by a GUI class without any effect on the ISPbean or
 * ISPBusinessImpl classes. Updated to make all objects declared in the
 * constructor as final.
 *
 * @author Ken Fogel
 * @version 1.1
 */
public class ISPConsolePresentationImpl implements ISPPresentation {

    private final Scanner sc;
    private final ISPBean ispBean;
    private final ISPBusiness ispBusiness;

    private final static Logger LOG = LoggerFactory.getLogger(ISPConsolePresentationImpl.class);

    /**
     * Non-default Constructor
     *
     * @param ispBean
     * @param ispBusiness
     */
    public ISPConsolePresentationImpl(ISPBean ispBean, ISPBusiness ispBusiness) {
        super();
        LOG.info("start");
        this.ispBean = ispBean;
        this.ispBusiness = ispBusiness;
        sc = new Scanner(System.in, "UTF-8");
        LOG.info("end");

    }

    /**
     * Request from the user the letter of the plan they are using
     */
    private void requestPlan() {
        LOG.info("start");

        System.out.println("Select an Internet Service plan (A/B/C)");

        // Primary Read
        char plan;
        if (sc.hasNext(REGEX_CHOICES)) { // Regular expression that only accepts a
            // single letter
            plan = sc.next().toUpperCase().charAt(0);
        } else {
            plan = INVALID;
        }
        sc.nextLine();

        // Secondary Read
        while (plan == INVALID) {
            System.out.println("Sorry, your plan choice is invalid.");
            System.out.println("Select an Internet Service plan (A/B/C)");
            if (sc.hasNext(REGEX_CHOICES)) {
                plan = sc.next().toUpperCase().charAt(0);
            }
            sc.nextLine();
        }
        ispBean.setPlanType(plan);
        LOG.info("end");
    }

    /**
     * Request the total number of hours, between 0 - 744 inclusive, that are
     * used
     */
    private void requestHours() {
        LOG.info("start");

        double hours;

        // Primary Read
        System.out.println("Enter the number of hours you used for the month.");
        if (sc.hasNextDouble()) {
            hours = sc.nextDouble();
        } else {
            hours = -1.0;
        }
        sc.nextLine();

        // Secondary Read
        while (hours < 0.0 || hours > 744.0) {
            System.out.println("Sorry, your input is invalid.");
            System.out
                    .println("Enter the number of hours you used for the month.");
            if (sc.hasNextDouble()) {
                hours = sc.nextDouble();
            }
            sc.nextLine();
        }
        ispBean.setTotalHours(hours);
        LOG.info("end");
    }

    /**
     * Display the billing report
     */
    private void displayResults() {

        LOG.info("start");

        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
        NumberFormat hoursFormat = NumberFormat.getNumberInstance();
        hoursFormat.setMaximumFractionDigits(1);
        hoursFormat.setMinimumFractionDigits(1);

        ispBusiness.updateISPBean();

        System.out.println("                    Plan Type: "
                + ispBean.getPlanType());
        System.out.println("       Included Hours in Plan: "
                + hoursFormat.format(ispBean.getIncludedHours()));
        System.out.println("             Additional Hours: "
                + hoursFormat.format(ispBusiness.getAdditionalHours()));
        System.out.println("                  Total Hours: "
                + hoursFormat.format(ispBean.getTotalHours()));
        System.out.println();
        System.out.println("                    Plan Rate: "
                + currencyFormat.format(ispBean.getPlanCost()));
        System.out.println("       Additional Hourly Rate: "
                + currencyFormat.format(ispBean.getHourlyCost()));
        System.out.println("            Total Hourly Cost: "
                + currencyFormat.format(ispBusiness.getAdditionalHoursCharge()));
        System.out.println();
        System.out.println("                   Total Cost: "
                + currencyFormat.format(ispBusiness.getTotalCost()));

        LOG.info("end");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kenfogel.isp.presentation.ISPPresentation#perform() The
     * operations in this console interface must occur in a specific order
     */
    @Override
    public void perform() {
        LOG.info("start");
        requestPlan();
        requestHours();
        displayResults();
        LOG.info("end");
    }
}
