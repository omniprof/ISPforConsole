package com.kenfogel.isp.presentation;

/**
 *
 * Common interface for the different presentation classes.
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public interface ISPPresentation {

    /**
     * The public method all presentation classes in this system must implement.
     */
    public void perform();
}
