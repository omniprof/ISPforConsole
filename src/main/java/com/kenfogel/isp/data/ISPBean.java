package com.kenfogel.isp.data;

/**
 * The java bean for the billing information. Avoid placing logic in data beans
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public class ISPBean {

    private char planType;
    private double planCost;
    private double totalHours;
    private double hourlyCost;
    private double includedHours;

    public ISPBean() {
        planType = 'A';
        planCost = 9.95;
        includedHours = 10.0;
    }

    public ISPBean(char planType, double planCost, double totalHours) {
        this.planType = planType;
        this.planCost = planCost;
        this.totalHours = totalHours;
    }

    public char getPlanType() {
        return planType;
    }

    public void setPlanType(char planType) {
        this.planType = planType;
    }

    public double getPlanCost() {
        return planCost;
    }

    public void setPlanCost(double planCost) {
        this.planCost = planCost;
    }

    public double getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(double totalHours) {
        this.totalHours = totalHours;
    }

    public double getHourlyCost() {
        return hourlyCost;
    }

    public void setHourlyCost(double hourlyCost) {
        this.hourlyCost = hourlyCost;
    }

    public double getIncludedHours() {
        return includedHours;
    }

    public void setIncludedHours(double includedHours) {
        this.includedHours = includedHours;
    }

    @Override
    public String toString() {
        return "ISPBean [planType=" + planType + ", planCost=" + planCost
                + ", totalHours=" + totalHours + ", hourlyCost=" + hourlyCost
                + ", includedHours=" + includedHours + "]";
    }
}
