package com.kenfogel.isp.business;

import static com.kenfogel.isp.constants.ISPConstants.ADDCOST_A;
import static com.kenfogel.isp.constants.ISPConstants.ADDCOST_B;
import static com.kenfogel.isp.constants.ISPConstants.INCLUDEDHOURS_A;
import static com.kenfogel.isp.constants.ISPConstants.INCLUDEDHOURS_B;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_A;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_B;
import static com.kenfogel.isp.constants.ISPConstants.PLANRATE_C;

import com.kenfogel.isp.data.ISPBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Business class Encapsulates all logic in the program Decoupled from other
 * layers
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public class ISPBusinessImpl implements ISPBusiness {

    private final ISPBean ispBean;
    private final static Logger LOG = LoggerFactory.getLogger(ISPBusinessImpl.class);

    /**
     * Default constructor that creates a default instance of the ISPBean
     */
    public ISPBusinessImpl() {
        ispBean = new ISPBean();
    }

    /**
     * Non-default constructor Shares the same ISPBean as the presentation layer
     *
     * @param ispBean
     */
    public ISPBusinessImpl(ISPBean ispBean) {
        this.ispBean = ispBean;
    }

    /**
     * Based on the plan chosen this sets the appropriate values in the ISPBean
     */
    @Override
    public void updateISPBean() {
        LOG.info("start");
        switch (ispBean.getPlanType()) {
            case 'A':
                ispBean.setPlanCost(PLANRATE_A);
                ispBean.setHourlyCost(ADDCOST_A);
                ispBean.setIncludedHours(INCLUDEDHOURS_A);
                break;
            case 'B':
                ispBean.setPlanCost(PLANRATE_B);
                ispBean.setHourlyCost(ADDCOST_B);
                ispBean.setIncludedHours(INCLUDEDHOURS_B);

                break;
            case 'C':
                ispBean.setPlanCost(PLANRATE_C);
                ispBean.setHourlyCost(0);
                ispBean.setIncludedHours(0);
                break;
            default:
                break;
        }
        LOG.info("end");
    }

    /**
     * Calculates the number of hours to be charged for, except for plan C and
     * returns the value
     *
     * @return number of chargeable hours
     */
    @Override
    public double getAdditionalHours() {
        LOG.info("start");

        double retVal = 0.0;

        if (ispBean.getPlanType() != 'C' && ispBean.getTotalHours() > ispBean.getIncludedHours()) {
            retVal = ispBean.getTotalHours() - ispBean.getIncludedHours();
        }
        LOG.info("end");
        return retVal;
    }

    /**
     * Calculates the charge for the additional hours
     *
     * @return the chargeable amount
     */
    @Override
    public double getAdditionalHoursCharge() {
        return getAdditionalHours() * ispBean.getHourlyCost();
    }

    /**
     * Calculates the total charge
     *
     * @return the total charge
     */
    @Override
    public double getTotalCost() {
        return getAdditionalHoursCharge() + ispBean.getPlanCost();
    }
}
