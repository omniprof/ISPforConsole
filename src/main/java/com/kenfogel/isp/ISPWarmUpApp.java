package com.kenfogel.isp;

import com.kenfogel.isp.business.ISPBusiness;

import com.kenfogel.isp.business.ISPBusinessImpl;
import com.kenfogel.isp.data.ISPBean;
import com.kenfogel.isp.presentation.ISPConsolePresentationImpl;
import com.kenfogel.isp.presentation.ISPPresentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The app class for a sample console program that demonstrates a layered
 * architecture Updated to use logger and junit testing
 *
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class ISPWarmUpApp {

    private final static Logger LOG = LoggerFactory.getLogger(ISPWarmUpApp.class);

    private ISPBusiness ispBusiness;
    private ISPBean ispBean;
    private ISPPresentation ispPresentation;

    /**
     * Constructor instantiates the required classes and transfers control
     */
    public ISPWarmUpApp() {
        super();

    }

    public void perform() {

        LOG.info("start");
        ispBean = new ISPBean();
        ispBusiness = new ISPBusinessImpl(ispBean);

        ispPresentation = new ISPConsolePresentationImpl(ispBean, ispBusiness);
        ispPresentation.perform();
        LOG.info("end");
    }

    /**
     * Start the app
     *
     * @param args
     */
    public static void main(final String[] args) {

        ISPWarmUpApp isp = new ISPWarmUpApp();
        isp.perform();
    }
}
